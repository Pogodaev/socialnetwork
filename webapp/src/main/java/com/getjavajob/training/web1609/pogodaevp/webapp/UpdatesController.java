package com.getjavajob.training.web1609.pogodaevp.webapp;

import org.apache.commons.io.FileUtils;
import com.getjavajob.training.web1609.pogodaevp.common.Account;
import com.getjavajob.training.web1609.pogodaevp.service.ServiceException;
import com.getjavajob.training.web1609.pogodaevp.service.UserService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialBlob;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;


@Controller
public class UpdatesController implements ServletContextAware {

    private static final Logger logger = LoggerFactory.getLogger(UpdatesController.class);

    @Autowired
    private UserService s;
    private ServletContext servletContext;

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }


    @RequestMapping(value = "/pregisterAccount", method = RequestMethod.GET)
    public ModelAndView gotoPage(HttpSession session) {
        ModelAndView model = null;
        model = new ModelAndView("registerPage");
        model.addObject("account", session.getAttribute("a"));
        return model;
    }

    @RequestMapping(value = "/tuploUpdates", method = RequestMethod.POST)
    public ModelAndView doGetThis(HttpServletRequest request, @ModelAttribute("account") Account acc, BindingResult result, @RequestParam(value = "photo", required = false) MultipartFile file) throws ServletException, IOException {
        Blob my = null;
        ModelAndView model = new ModelAndView("fail");

        if (result.hasErrors()) {
        }

        logger.info("We now in /tuploUpdates POST");

        HttpSession session = request.getSession();
        if (!file.isEmpty()) {
            byte[] data = file.getBytes();
            try {
                my = new SerialBlob(data);
            } catch (SQLException e) {
                logger.error("Cann't load your PHOTO" + e.getMessage());
                throw new ServletException(e);
            }
            session.setAttribute("image", my);
        } else {
            my = (Blob) session.getAttribute("image");
        }

        String tmp = acc.getPassword();

        session.setAttribute("phones", acc.getPhons());

        String password = "";
        if (tmp != null && !tmp.equals("")) {
            password = Sha256.sha256(tmp);
        } else {
            password = (String) session.getAttribute("user_pass");
        }

        if (acc.getFirstName() != null && acc.getEmail() != null) {

            acc.setPassword(password);
            acc.setImg(my);

            if (acc.getId() == null) { //Register
                logger.info("Going to register new Account");
                Account tyt = null;
                try {
                    tyt = s.createAccount(acc, acc.getPhons());
                } catch (ServiceException e) {
                    throw new ServletException(e);
                }
                session.setAttribute("user", "TRUE");
                session.setAttribute("session", "TRUE");
                session.setAttribute("a", tyt);
                session.setAttribute("account", tyt);
                session.setAttribute("uId", tyt.getId());
                model = new ModelAndView("accountPage");
                model.addObject("account", tyt);
                model.addObject("phones", tyt.getPhons());

            } else {
                try {
                    s.updateAccount(acc, acc.getPhons());
                } catch (ServiceException e) {
                    throw new ServletException(e);
                }
                session.setAttribute("a", acc);
                model = new ModelAndView("accountPage");
                model.addObject("account", acc);
            }
            return model;
        }
        return model;
    }
}
