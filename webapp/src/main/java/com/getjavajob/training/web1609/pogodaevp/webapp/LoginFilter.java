package com.getjavajob.training.web1609.pogodaevp.webapp;



import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


@WebFilter("/*")
public class LoginFilter  implements Filter {

    final static String[] pagesArray = {"css", "jpeg", "jpg", "/pregisterAccount", "/tuploUpdates", "/test", "js", "/", ""};

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);

        Set<String> set = new HashSet(Arrays.asList(pagesArray));

        String contextPath = request.getContextPath();
        String loginURI = contextPath + "/login";
        String match = contextPath + "/start";
        String register = contextPath + "/pregisterAccount";

        String reqURI = request.getRequestURI();


        boolean registration = reqURI.equals(register);
        boolean loggedIn = (session != null) && (session.getAttribute("user") != null);
        boolean loginRequest = reqURI.equals(loginURI);
        boolean startPage = reqURI.equals(match);
        boolean notLogged = (session != null) && (session.getAttribute("user") == null);

        boolean css = false;
        for (String s : set) {
            if (reqURI.endsWith(s)) {
                css = true;
                break;
            }
        }

        if ((loggedIn || loginRequest) || startPage || css || registration) {
                filterChain.doFilter(request, response);
        } else if (session == null || notLogged ) {
            request.setAttribute("help", "yes");
            request.getRequestDispatcher("/login").forward(request, response);
        } else {
            response.sendRedirect(match);
        }

    }

    @Override
    public void destroy() {

    }
}
