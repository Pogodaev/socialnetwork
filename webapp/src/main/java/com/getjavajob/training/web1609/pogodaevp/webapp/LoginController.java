package com.getjavajob.training.web1609.pogodaevp.webapp;

import com.getjavajob.training.web1609.pogodaevp.common.Account;
import com.getjavajob.training.web1609.pogodaevp.service.FriendsService;
import com.getjavajob.training.web1609.pogodaevp.service.ServiceException;
import com.getjavajob.training.web1609.pogodaevp.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.Blob;
import java.util.List;


@Controller
public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    UserService userService;
    @Autowired
    FriendsService friendsService;



    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView init(HttpServletRequest request, HttpSession session) throws ServletException {
        logger.info("I'm in /login GET controller ");
        Blob img = null;
        Account atAccount = null;
        List<String> phones = null;
        String email = "";
        String password = "";

        ModelAndView model = null;

//        atAccount = getCurrentUser();
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (c.getName().equals("user")) {
                    email = c.getValue();
                }
                if (c.getName().equals("user_pass")) {
                    password = c.getValue();
                }
            }
        }


        if (!validate(email, password)) {
            try {
                atAccount = userService.searchByEmailPassword(email, password);
                logger.info("Found %s Account in GET /login", atAccount);
            } catch (ServiceException e) {
                logger.error("Didn't found Account  " + e.getMessage(), e);
                throw new ServletException(e);
            }
        } else {
            logger.info("Failed to find cookies in GET /login, going to /fail page");
            model = new ModelAndView("fail");
            return model;
        }


        if (atAccount != null) {
            img = atAccount.getImg();
            Long noticeCount = friendsService.getUserNoticeCount(atAccount.getId());
            Long friendsCount = friendsService.getUserFriendsCount(atAccount.getId());
            try {
                phones = userService.listPhoneNumbers(atAccount.getIdAccount());
            } catch (ServiceException e) {
                throw new ServletException(e);
            }
            session.setAttribute("user", "TRUE");
            session.setMaxInactiveInterval(10 * 60);
            session.setAttribute("session", "TRUE");
            session.setAttribute("img", img);
            session.setAttribute("image", img);
            session.setAttribute("phones", phones);
            session.setAttribute("uId", atAccount.getId());
            session.setAttribute("nCount", noticeCount);
            session.setAttribute("fCount", friendsCount);
            session.setAttribute("a", atAccount);
            model = new ModelAndView("accountPage");
            model.addObject("account", atAccount);
            model.addObject("phones", phones);
            logger.debug("Going from /login GET to /account page with %s ", atAccount);
            return model;
        }
        return model;
    }


    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView loginPage(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("loginBean")LoginBean loginBean) throws ServletException {
        boolean chk = request.getParameter("check") != null;

        logger.debug("I'm in /login POST %s ", loginBean.toString());
        String email = loginBean.getEmail();
        String password = Sha256.sha256(loginBean.getPassword());
        Account account = null;
        Blob img = null;
        Account atAccount = null;


        try {
            atAccount = userService.searchByEmailPassword(email, password);
            logger.info("Found %s Account in POST /login" + atAccount.toString());
        } catch (ServiceException e) {
            throw new ServletException(e);
        }

        if (atAccount != null) {
            account = atAccount;
            Long noticeCount = friendsService.getUserNoticeCount(atAccount.getId());
            Long friendsCount = friendsService.getUserFriendsCount(atAccount.getId());
            img = atAccount.getImg();
            List<String> phones = null;
            try {
                phones = userService.listPhoneNumbers(atAccount.getIdAccount());
            } catch (ServiceException e) {
                throw new ServletException(e);
            }
            HttpSession session = request.getSession();
            session.setAttribute("user", email);
            session.setAttribute("user_pass", password);
            session.setMaxInactiveInterval(10 * 60);
            if (chk) {
                Cookie loginCookie = new Cookie("user", email);
                Cookie passCookie = new Cookie("user_pass", password);
                response.addCookie(loginCookie);
                response.addCookie(passCookie);
            }
            session.setAttribute("session", "TRUE");
            session.setAttribute("img", img);
            session.setAttribute("image", img);
            session.setAttribute("phones", phones);
            session.setAttribute("uId", account.getId());
            session.setAttribute("nCount", noticeCount);
            session.setAttribute("fCount", friendsCount);
            session.setAttribute("a", account);
            logger.debug("Going from /login POST to /account page with %s ", atAccount.toString());
            ModelAndView model = new ModelAndView("accountPage");
            model.addObject("account", account);

            return model;
        } else {
            return new ModelAndView("fail");
        }
    }

    @RequestMapping(value = "/start")
    public String firstPage() {
        return "startPage";
    }

    @RequestMapping(value = "/fail")
    public String failPage() {
        logger.info("Going to /fail page ");
        return "fail";
    }



    public boolean validate(String email, String password) {
        return (email == null || email.isEmpty()) || (password == null || password.isEmpty());
    }

    @RequestMapping(value = "/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        logger.info("Going to logout ");
        session.setAttribute("user", null);
        session.invalidate();

        Cookie loginCookie = new Cookie("user", "");
        Cookie passCookie = new Cookie("user_pass", "");
        loginCookie.setMaxAge(0);
        passCookie.setMaxAge(0);
        response.addCookie(loginCookie);
        response.addCookie(passCookie);
        return "startPage";
    }
}
