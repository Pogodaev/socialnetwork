package com.getjavajob.training.web1609.pogodaevp.webapp;

import com.getjavajob.training.web1609.pogodaevp.common.Account;
import com.getjavajob.training.web1609.pogodaevp.common.AccountDTO;
import com.getjavajob.training.web1609.pogodaevp.service.ServiceException;
import com.getjavajob.training.web1609.pogodaevp.service.UserService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialBlob;
import java.awt.image.BufferedImage;
import java.io.*;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;


@Controller
public class SearchController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/searching")
    @ResponseBody
    public List<AccountDTO> searchGuys(@RequestParam("searchs") final String filter) throws ServletException {

        List<Account> list;
        List<AccountDTO> listres = new LinkedList<>();
        try {
             list = userService.getAllUsers();
             for (Account a : list) {
                    listres.add(new AccountDTO(a));
             }
        } catch (ServiceException e) {
            throw new ServletException(e);
        }
        CollectionUtils.filter(listres, new Predicate<AccountDTO>() {
            @Override
            public boolean evaluate(AccountDTO account) {
                return account.getFirstName().contains(filter) || account.getLastName().contains(filter);
            }
        });
        return listres;
    }

    @RequestMapping(value = "/searchEmail", method = RequestMethod.GET)
    public ModelAndView searchBy(HttpServletRequest request, @RequestParam("sString") String res) throws ServletException {
        HttpSession session = request.getSession();

        Account acc = null;
        try {
            acc = userService.searchByEmail(res).get(0);
        } catch (ServiceException e) {
            throw new ServletException(e);
        }
        ModelAndView model = new ModelAndView("simpleView");
        model.addObject("result", acc);
        session.setAttribute("hello", acc.getImg());
        model.addObject("hello", acc.getImg());
        return model;
    }

    @RequestMapping(value = "/searchEmail", method = RequestMethod.POST)
    public ModelAndView search(HttpServletRequest request, @RequestParam("sString") String res) throws ServletException {


        Long k =  userService.getCountGuys(res);

        ModelAndView model = new ModelAndView("resultSearchPage");
        model.addObject("count", k);
        model.addObject("pattern", res);

        return model;
    }

    @RequestMapping(value = "/blob", method = RequestMethod.GET)
    public void blobConvert(HttpSession session, HttpServletResponse resp, HttpServletRequest req) throws IOException, ServletException {

        Blob blob = null;
        blob = (Blob) session.getAttribute("hello");
        if (blob == null) {
            blob = (Blob) session.getAttribute("image");
        }

        byte[] def = null;
        if (blob == null) {
            String relativeWebPath = "/resources/img/default-no-image.jpg";
            String root = session.getServletContext().getRealPath(relativeWebPath);
            InputStream is = new FileInputStream(new File(root));
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            if (is.available() > 0) {
                int next = is.read();
                while (next > -1) {
                    bos.write(next);
                    next = is.read();
                }
                bos.flush();
                def = bos.toByteArray();
            }

        }

        byte[] image = null;
        if (blob != null) {
            try {
                image = blob.getBytes(1, (int) blob.length());
            } catch (SQLException e) {
                throw new ServletException(e);
            }
        }

        if (image == null ) {
            image = def;
        }

        session.setAttribute("hello", null);
        OutputStream out = resp.getOutputStream();
        out.write(image);
        out.close();
        out.flush();
    }

    @RequestMapping(value = "/process")
    @ResponseBody
    public String process(HttpServletRequest request, @RequestParam("requestType")  String req, @RequestParam("match")  String patt) throws ServletException {
        JSONObject obj = new JSONObject();
        JSONArray arr = new JSONArray();
        String data = "";
        if (req.equals("countRecords")) {
            obj.put("count", userService.getCountGuys(patt));
        }

        List<Account> rlist = new LinkedList<>();
        if (req.equals("getRecords")) {
            int start = Integer.parseInt(request.getParameter("currentIndex"));
            int total = Integer.parseInt(request.getParameter("recordsToFetch"));
            String pattern = request.getParameter("match");
            rlist = userService.getRecords(start, total, pattern);
        }
        for (Account a : rlist) {
            arr.add(a.getFirstName() + " " + a.getLastName() + " " + a.getEmail());
        }
        obj.put("record", arr);

        return obj.toString();
    }
}
