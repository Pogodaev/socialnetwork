package com.getjavajob.training.web1609.pogodaevp.webapp;

import com.getjavajob.training.web1609.pogodaevp.common.Account;
import com.getjavajob.training.web1609.pogodaevp.service.FriendsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class ListFriendsController {

    private static final Logger logger = LoggerFactory.getLogger(ListFriendsController.class);
    @Autowired
    FriendsService friendsService;

    @RequestMapping(value = "/userFriends", method = RequestMethod.GET)
    public ModelAndView friendsList(HttpSession session) {
        int id = (int) session.getAttribute("uId");
        logger.info("I'm in user friends controller " + id);
        ModelAndView model = new ModelAndView("friendsPage");
        model.addObject("uFriends", friendsService.getUserFriends(id));
        return model;
    }

    @RequestMapping(value = "/listNotices", method = RequestMethod.GET)
    public ModelAndView noticesList(HttpServletRequest request) {
        int id = Integer.valueOf(request.getParameter("value"));
        List<Account> list = friendsService.getNoticeFriendsOfUser(id);
        logger.info("I'm in listNotices controller " + id);
        ModelAndView model = null;
        if (list.size() == 0) {
            logger.info("Going to AccountPage");
            model = new ModelAndView("accountPage");
        } else {
            logger.info("Going to noticePage");
            model = new ModelAndView("noticePage");
            model.addObject("notices", list);
        }
        return model;
    }
}
