package com.getjavajob.training.web1609.pogodaevp.webapp;

import com.getjavajob.training.web1609.pogodaevp.service.FriendsService;
import com.getjavajob.training.web1609.pogodaevp.service.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class FriendAddDeleteController {

    @Autowired
    FriendsService friendsService;

    private static final Logger logger = LoggerFactory.getLogger(FriendAddDeleteController.class);

    @RequestMapping(value = "/addFriend", method = RequestMethod.GET)
    public ModelAndView add(HttpServletRequest request) throws ServletException {

        logger.info("i'm in /addFriend GET servlet ");
        HttpSession session = request.getSession();
        int uId = (int) session.getAttribute("uId");
        int idAccount = Integer.valueOf(request.getParameter("value"));

        try {
            logger.debug("Sending notice to user " + uId + " " + idAccount);
            friendsService.sendNoticeTo(uId, idAccount);
        } catch (ServiceException e) {
            logger.debug("All bad " + e.getMessage());
            throw new ServletException(e);
        }
        ModelAndView model = new ModelAndView("accountPage");
        return model;
    }

    @RequestMapping(value = "/deleteFriend", method = RequestMethod.GET)
    public ModelAndView delete(HttpServletRequest request) throws ServletException {
        logger.info("i'm in /deleteFriend GET servlet ");

        HttpSession session = request.getSession();
        long fCount = (long) session.getAttribute("fCount");
        int idDel = Integer.valueOf(request.getParameter("value"));
        int uId = (int) session.getAttribute("uId");
        try {
            logger.debug("Deleting friend " + uId + " " + idDel);
            friendsService.deleteFriend(uId, idDel);
        } catch (ServiceException e) {
            throw new ServletException(e);
        }
        session.setAttribute("fCount", --fCount);
        request.setAttribute("value", uId);
        logger.info("Going to friendsPage ");
        ModelAndView model = new ModelAndView("friendsPage");
        model.addObject("uFriends", friendsService.getUserFriends(uId));
        return model;
    }
}
