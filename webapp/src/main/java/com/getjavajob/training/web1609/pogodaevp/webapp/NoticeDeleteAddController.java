package com.getjavajob.training.web1609.pogodaevp.webapp;

import com.getjavajob.training.web1609.pogodaevp.service.FriendsService;
import com.getjavajob.training.web1609.pogodaevp.service.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class NoticeDeleteAddController {

    private static final Logger logger = LoggerFactory.getLogger(NoticeDeleteAddController.class);

    @Autowired
    private ApplicationContext context;

    @Autowired
    private FriendsService friendsService;

    @RequestMapping(value = "/acceptNotice", method = RequestMethod.GET)
    public ModelAndView accept(HttpServletRequest request) throws ServletException {
        logger.info("I'm in /acceptNotice GET controller ");
        HttpSession session = request.getSession();
        int uId = (int) session.getAttribute("uId");
        int idAccount = Integer.valueOf(request.getParameter("value"));
        long nCount = (long) session.getAttribute("nCount");

        try {
            logger.debug("acceptFriendship done");
            friendsService.acceptFriendship(idAccount, uId);
        } catch (ServiceException e) {
            throw new ServletException(e);
        }
        long fCount = (long) session.getAttribute("fCount");
        session.setAttribute("nCount", --nCount);

        ModelAndView model = null;
        if (nCount == 0) {
             session.setAttribute("fCount", ++fCount);
             model = new ModelAndView("accountPage");
        } else {
             model = new ModelAndView("noticePage");
        }
        return model;
    }

    @RequestMapping(value = "/rejectNotice", method = RequestMethod.GET)
    public ModelAndView reject(HttpServletRequest request) throws ServletException {
        logger.info("I'm in /rejectNotice GET controller ");
        HttpSession session = request.getSession();
        int idAccount = Integer.valueOf(request.getParameter("value"));
        int uId = (int) session.getAttribute("uId");
        long nCount = (long) session.getAttribute("nCount");
        try {
            friendsService.rejectFriendship(idAccount, uId);
        } catch (ServiceException e) {
            logger.error("Service exception " + e.getMessage());
            throw new ServletException(e);
        }
        session.setAttribute("nCount", --nCount);
        ModelAndView model = null;
        if (nCount == 0) {
            model = new ModelAndView("accountPage");
        } else {
            model = new ModelAndView("noticePage");
        }
        return model;
    }
}
