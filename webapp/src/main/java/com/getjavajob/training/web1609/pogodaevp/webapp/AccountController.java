package com.getjavajob.training.web1609.pogodaevp.webapp;

import com.getjavajob.training.web1609.pogodaevp.common.Account;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;


@Controller
public class AccountController {

    @ModelAttribute("account")
    public Account getAcc(HttpSession session) {
        Account a = (Account) session.getAttribute("a");
        return a;
    }

    @RequestMapping(value = "/account")
    public String initAccount() {
        return "accountPage";
    }
}
