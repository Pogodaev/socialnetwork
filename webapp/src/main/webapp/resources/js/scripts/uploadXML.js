/**
 * Created by paul on 24.05.17.
 */
$(document).ready(function(){
    var aTags = document.getElementById("selectfile");
    aTags.addEventListener("change", doit);
});

function doit(e) {
    var files = e.target.files;
    var reader = new FileReader();
    reader.onload = function() {
        var parsed = new DOMParser().parseFromString(this.result, "text/xml");
        console.log(parsed);

        $("#fname").val(parsed.getElementsByTagName("firstName")[0].childNodes[0].nodeValue);
        $("#lname").val(parsed.getElementsByTagName("lastName")[0].childNodes[0].nodeValue);
        $("#mname").val(parsed.getElementsByTagName("middleName")[0].childNodes[0].nodeValue);
        $("#email").val(parsed.getElementsByTagName("email")[0].childNodes[0].nodeValue);
        $("#skype").val(parsed.getElementsByTagName("skype")[0].childNodes[0].nodeValue);
        $("#icq").val(parsed.getElementsByTagName("icq")[0].childNodes[0].nodeValue);
        $("#home").val(parsed.getElementsByTagName("homeAddress")[0].childNodes[0].nodeValue);
        $("#datepicker").val(parsed.getElementsByTagName("dateBirth")[0].childNodes[0].nodeValue);
        $("#add").val(parsed.getElementsByTagName("additional")[0].childNodes[0].nodeValue);
        $("#job").val(parsed.getElementsByTagName("jobAddress")[0].childNodes[0].nodeValue);
        var numbers = parsed.getElementsByTagName("ph");
        $("#dTable > li").remove();

        for ( var i = 0, l = numbers.length; i < l; i++ ) {
            addRow(numbers[i].childNodes[0].nodeValue)
        }

    };
    reader.readAsText(files[0]);
}
