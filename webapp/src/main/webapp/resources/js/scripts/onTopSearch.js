/**
 * Created by paul on 24.05.17.
 */
$( function() {
    var query = document.getElementById('query').value;
    var searchString = document.getElementById('lookFor').value;
    $( "#tags" ).autocomplete({
        source: function( request, response ) {
            $.ajax( {
                url: query,
                data: {
                    searchs: request.term
                },
                success: function( data ) {
                    response($.map( data, function (accountdto, i) {
                        var ml = accountdto.email;
                        var fn = accountdto.firstName;
                        return {value : " ", mail : ml, label :  fn + " " + ml}
                    }));
                },
            });
        },
        select : function ( event, ui ) {
            window.location = searchString + ui.item.mail;
        },
        minLength: 2
    } );
} );