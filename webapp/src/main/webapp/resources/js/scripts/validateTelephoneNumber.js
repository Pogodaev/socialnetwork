
function validate(number) {
    var match = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    if (number.value.match(match)) {
        addRow(number);
        return true;
    } else {
        alert("Bad number Format");
        return false;
    }
}