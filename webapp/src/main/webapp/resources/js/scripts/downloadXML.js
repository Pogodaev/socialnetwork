/**
 * Created by paul on 24.05.17.
 */
$(document).ready(function(){
    var aTags = document.getElementById("selectfile");
    aTags.addEventListener("change", doit);
//todo document.getElementById to (#selectfile)
    data = [];
    data.push("\<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
    data.push("\<account\>\n   ");
    data.push("\<firstName>"+ document.getElementById('fname').value + "\</firstName>\n   ");
    data.push("\<lastName>"+ document.getElementById('lname').value + "\</lastName>\n   ");
    data.push("\<middleName>"+ document.getElementById('mname').value + "\</middleName>\n   ");
    data.push("\<phons>\n");
    numbers = document.getElementsByName("phons");
    for ( var i = 0, l = numbers.length; i < l; i++ ) {
        data.push("      \<ph>"+numbers[i].value+"\</ph>\n");
    }
    data.push("   \</phons>\n   ");
    data.push("\<homeAddress>"+document.getElementById('home').value+"\</homeAddress>\n   ");
    data.push("\<jobAddress>"+document.getElementById('job').value+"\</jobAddress>\n   ");
    data.push("\<email>"+document.getElementById('email').value+"\</email>\n   ");
    data.push("\<icq>"+document.getElementById('icq').value+"\</icq>\n   ");
    data.push("\<skype>"+document.getElementById('skype').value+"\</skype>\n   ");
    data.push("\<additional>"+document.getElementById('add').value+"\</additional>\n   ");
    data.push("\<dateBirth>"+document.getElementById('datepicker').value+"\</dateBirth>\n");
    data.push("\</account>");
    properties = {type: 'plain/xml'};
    try {
        // Specify the filename using the File constructor, but ...
        file = new File(data, "file.xml", properties);
    } catch (e) {
        // ... fall back to the Blob constructor if that isn't supported.
        file = new Blob(data, properties);
    }
    url = URL.createObjectURL(file);
    document.getElementById("link").href = url;
});