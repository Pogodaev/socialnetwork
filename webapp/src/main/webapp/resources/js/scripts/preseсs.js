/**
 * Created by paul on 29.05.17.
 */
/**
 * Created by paul on 24.05.17.
 */
$(document).ready(function(){
    var totalRecords;
    var recordsPerPage=5;
    var recordsToFetch=recordsPerPage;
    var totalPages;
    var currentPage=1;
    var currentIndex=0;
    var pattern=document.getElementById('match').value;

    $.get("process?requestType=countRecords&match="+pattern,function(data){
        var JSONData = JSON.parse(data);
        totalRecords = JSONData.count;
        totalPages = Math.floor(totalRecords / recordsPerPage);

        if (totalRecords % recordsPerPage != 0) {
            totalPages++;
        }

        if (totalRecords < recordsPerPage) {
            recordsToFetch = totalRecords % recordsPerPage;
        } else {
            recordsToFetch = recordsPerPage;
        }

        $("#page").html("Page " + currentPage + " of " + totalPages);
    });

    $.get("process?requestType=getRecords&currentIndex="+currentIndex+"&recordsToFetch="+recordsToFetch+"&match="+pattern,function(data){
        var JSONData = JSON.parse(data);

        for (i = 0; i < recordsToFetch; ++i) {

            $("#div1").append("<tr><td>" + (currentIndex + 1) + ". </td><td>" + JSONData.record[i].split(" ")[0] + "</td><td>"+ JSONData.record[i].split(" ")[1] + "</td><td><a href=\"searchEmail?sString="+JSONData.record[i].split(" ")[2]+"\">"+JSONData.record[i].split(" ")[2] +"</a></td> </tr>");
            currentIndex++;
        }
        if (currentPage == totalPages) {
            $("#next").hide();
        } else {
            $("#next").show();
        }

        if (currentPage == 1) {
            $("#back").hide();
        } else{
            $("#back").show();
        }
    });


    $("#next").click(function() {
        $("#div1").html("");
        currentPage++;

        if (currentPage == totalPages) {
            $("#next").hide();
            if (totalRecords % recordsPerPage != 0) {
                recordsToFetch = totalRecords % recordsPerPage;
            } else {
                recordsToFetch = recordsPerPage;
            }
        } else {
            $("#next").show();
            recordsToFetch = recordsPerPage;
        }

        if (currentPage == 1) {
            $("#back").hide();
        } else {
            $("#back").show();
        }

        var url = 'process';
        $.ajax({
            type : 'post',
            url : url,
            dataType : 'json',
            data : {
                'requestType' : 'getRecords',
                'currentIndex' : currentIndex,
                'recordsToFetch' : recordsToFetch,
                'match' : pattern
            },
            success : function(data) {
                for (i = 0; i < recordsToFetch; ++i) {
                    $("#div1").append("<tr><td>" + (currentIndex + 1) + ". </td><td>" + data.record[i].split(" ")[0] + "</td><td>"+ data.record[i].split(" ")[1] + "</td><td><a href=\"searchEmail?sString="+data.record[i].split(" ")[2]+"\">"+data.record[i].split(" ")[2]+"</a></td> </tr>");
                    currentIndex++;
                }
            },
            complete : function(data) {
                $("#page").html("Page " + currentPage + " of " + totalPages);
            }
        });
    });

    $("#back").click(function() {
        $("#div1").html("");
        currentPage--;
        currentIndex = currentIndex - recordsToFetch - recordsPerPage;

        if (currentPage == totalPages) {
            $("#next").hide();
            recordsToFetch = totalRecords % recordsPerPage;
        } else {
            $("#next").show();
            recordsToFetch = recordsPerPage;
        }

        if (currentPage == 1) {
            $("#back").hide();
        } else {
            $("#back").show();
        }

        var url = 'process';
        $.ajax({
            type : 'post',
            url : url,
            dataType : 'json',
            data : {
                'requestType' : 'getRecords',
                'currentIndex' : currentIndex,
                'recordsToFetch' : recordsToFetch,
                'match' : pattern
            },
            success : function(data) {
                for (i = 0; i < recordsToFetch; ++i) {
                    $("#div1").append("<tr><td>" + (currentIndex + 1) + ". </td><td>" + data.record[i].split(" ")[0] + "</td><td>"+ data.record[i].split(" ")[1] + "</td><td><a href=\"searchEmail?sString="+data.record[i].split(" ")[2]+"\">"+data.record[i].split(" ")[2]+"</a></td> </tr>");
                    currentIndex++;
                }
            },
            complete : function(data) {
                $("#page").html("Page " + currentPage + " of " + totalPages);
            }
        });
    });
});
/**
 * Created by paul on 29.05.17.
 */
