<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Friends</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.js"></script>
    <script src="<%=request.getContextPath()%>/resources/js/bootstrap.js"></script>
</head>
<body>
<div>
    <%@include file="search.jsp"%>
</div>
<form:form style="padding-top: 50px">
    <div class="container">
        <table class="table table-condensed">
            <thead>
            <tr>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Email</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="acc" items="${uFriends}">
                <tr>
                    <td>${acc.firstName}</td>
                    <td>${acc.lastName}</td>
                    <td><a href="searchEmail?sString=${acc.email}">${acc.email}</a></td>
                    <td><a href="deleteFriend?value=${acc.idAccount}">Delete</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</form:form>
</body>
</html>
