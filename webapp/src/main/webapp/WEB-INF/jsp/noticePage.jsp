<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
    <title>Notices</title>
</head>
<body>
<%@include file="search.jsp"%>

<form>
<div class="container" style="margin-top: 50px;">
    <table class="table table-condensed">
        <thead>
        <tr>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Email</th>
            <th>#</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="acc" items="${notices}">
            <tr>
                <td>${acc.firstName}</td>
                <td>${acc.lastName}</td>
                <td>${acc.email}</td>
                <td><a href="acceptNotice?value=${acc.idAccount}">Accept</a> </td>
                <td><a href="rejectNotice?value=${acc.idAccount}">Reject</a> </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</form>
</body>
</html>
