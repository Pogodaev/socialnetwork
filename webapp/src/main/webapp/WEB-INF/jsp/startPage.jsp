<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<html>
<head>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
    <title>SocialNetwork</title>
</head>
<body id="my">

    <nav class="navbar navbar-fixed-top navbar-inverse">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand">Social Network</a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav" >
                    <li class="active">
                        <a href="pregisterAccount">Register Here</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

<div class="container" align="center" style="margin-top: 30px; width: 30%">
    <form:form class="form-signin" method="post" action="login" modelattribute="account">
        <h2 class="form-signin-heading">Please Sign In</h2>
        <label class="sr-only" for="inputEmail">Email Address</label>
        <input id="inputEmail" class="form-control" placeholder="EmailAddress" required="" autofocus="" type="email" name="email" size="20px">
        <input id="inputPassword" class="form-control" placeholder="Password" required="" name="password" type="password" size="20px">
        <label class="sr-only" for="inputPassword">Password</label>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="check" value = "check">
                Remember Me
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form:form>
</div>

</body>

</html>