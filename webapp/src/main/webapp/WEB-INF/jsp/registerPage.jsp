
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>

    <!--link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/registerPage.css"-->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jquery.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/scripts/dateChooser.js"></script>

    <title>Registration</title>

    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/scripts/addTelRow.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/deleteTelephoneRow.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/scripts/validateTelephoneNumber.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/showInsureMessage.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/scripts/uploadXML.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/scripts/downloadXML.js"></script>

</head>


<body>

<%@include file="search.jsp"%>
<div class="container">
    <h1>Edit Profile</h1>
    <hr>
    <div class="row">
        <!-- left column -->
        <div class="col-md-3">
            <div class="text-center">
                <img src="blob" class="avatar img-circle"  alt="avatar" style="width: 100px; height: 100px;">
                <h6>Upload a different photo...</h6>
            </div>
        </div>
        <div class="col-md-9 personal-info">
        <h3>Personal info</h3>
        <form method="post" action="tuploUpdates"  name="form1" class="form-horizontal" enctype="multipart/form-data" >
            <div class="form-group" >
                <label class="col-lg-3 control-label">Photo:</label>
                <div class="col-lg-8" style="border-color: transparent">
                    <input id="photo" name="photo" class="form-control" type="file" style="border-color: transparent">
                </div>
            </div>
            <div class="form-group" >
                <label class="col-lg-3 control-label">First name:</label>
                <div class="col-lg-8">
                    <input type="hidden" name="id" value="${account.id}">
                    <input id="fname" name="firstName" class="form-control" value="${account.firstName}" type="text">
                </div>
            </div>
            <div class="form-group" >
                <label class="col-lg-3 control-label">Last Name</label>
                <div class="col-lg-8">
                    <input id="lname" name="lastName" class="form-control" value="${account.lastName}" type="text">
                </div>
            </div>
            <div class="form-group" >
                <label class="col-lg-3 control-label">Middle Name</label>
                <div class="col-lg-8">
                    <input id="mname" name="middleName" class="form-control" value="${account.middleName}" type="text">
                </div>
            </div>
            <div class="form-group" >
                <label class="col-lg-3 control-label">Email </label>
                <div class="col-lg-8">
                    <input id="email" name="email" class="form-control" value="${account.email}" type="text">
                </div>
            </div>
            <div class="form-group" >
                <label class="col-lg-3 control-label">Skype </label>
                <div class="col-lg-8">
                    <input id="skype" name="skype" class="form-control" value="${account.skype}" type="text">
                </div>
            </div>
            <div class="form-group" >
                <label class="col-lg-3 control-label">ICQ </label>
                <div class="col-lg-8">
                    <input id="icq" name="icq" class="form-control" value="${account.icq}" type="text">
                </div>
            </div>
            <div class="form-group" >
                <label class="col-lg-3 control-label">Home Address</label>
                <div class="col-lg-8">
                    <input id="home" name="homeAddress" class="form-control" value="${account.homeAddress}" type="text">
                </div>
            </div>
            <div class="form-group" >
                <label class="col-lg-3 control-label">Birth Date</label>
                <div class="col-lg-8">
                    <input class="form-control" name="dateBirth"  id="datepicker" value="${account.dateBirth}" type="text">
                </div>
            </div>
            <div class="form-group" >
                <label class="col-lg-3 control-label">About Me</label>
                <div class="col-lg-8">
                    <input id="add" name="additional" class="form-control" value="${account.additional}" type="text">
                </div>
            </div>
            <div class="form-group" >
                <label class="col-lg-3 control-label">Job Address</label>
                <div class="col-lg-8">
                    <input id="job" class="form-control" name="jobAddress" value="${account.jobAddress}" type="text">
                </div>
            </div>
            <div class="form-group" >
                <label class="col-lg-3 control-label">Password</label>
                <div class="col-lg-8">
                    <input id="pass" name="password" class="form-control" type="password" value="" >
                </div>
            </div>
            <div class="form-group" >
                <label class="col-lg-3 control-label">Phone</label>
                <div class="col-lg-8">
                    <input class="form-control" name="phns" type="text" value="" ><span></span><input type="button" value="add" onclick="validate(document.form1.phns)">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label"></label>
                <div class="col-md-8">
                    <input type="submit" id="submit" value="Accept" name="submit" hidden="true">
                    <input class="btn btn-primary" value="Save Changes" type="button" onclick="sure()">
                    <span></span>
                    <input class="btn btn-default" value="Cancel" type="reset">
                    <span></span>
                    <label for="selectfile">
                        <h>UploadXML</h>
                        <span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                        <input type="file" id="selectfile" style="display:none">
                    </label>
                    <span></span>
                    <a id="link" target="_blank" download="account.xml">DownloadXML<i class="glyphicon glyphicon-download"></i></a>
                    <!--input id="xml" name="xml" type="file" class="file" data-show-preview="true" value="DownloadXML" >DownloadXML <a id="link" target="_blank" download="account.xml"><i class="glyphicon glyphicon-download"></i></a> </input-->
                </div>
            </div>

            <h1>Phones</h1>
            <hr>
<div class="form-group" id="dTable">
            <c:forEach var="ph" items="${phones}">
        <div class="form-group" >
            <label class="col-md-3 control-label">Phone</label>
            <div class="col-md-8">
                <input name="phons" id="phones" value="${ph}"/>
                <span></span>
                <input type="button" value="X" onclick="deleteRow(this)">
            </div>
        </div>
            </c:forEach>
</div>
        </form>


    </form>
    </div>
    </div>

    </div>
    <hr>
</body>

</html>
