<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
    <title>SomeGuys</title>
</head>
<body>

<%@include file="search.jsp"%>
<form class="form-inline"  style="width: 100%;" >
<div class="form-group" style="width: 15%">
    <img src="blob" width="200" height="320">
    <ul class="list-group" >
        <li>
            <a href="addFriend?value=${result.idAccount}" class="list-group-item active">Send notice</a>
        </li>
        <li>
            <a href="deleteFriend?value=${result.idAccount}" class="list-group-item active">Delete</a>
        </li>
        <li class="list-group-item list-group-item-info">
            <strong class="list-group-item-heading">First Name</strong>
            <p class="list-group-item-text">${result.firstName}</p>
        </li>
        <li class="list-group-item list-group-item-info">
            <strong class="list-group-item-heading">Last Name</strong>
            <p class="list-group-item-text">${result.lastName}</p>
        </li>
        <li class="list-group-item list-group-item-info">
            <strong class="list-group-item-heading">Email</strong>
            <p class="list-group-item-text">${result.email}</p>
        </li>
    </ul>
</div>

        <!--table border="0.5" width="35%" cellpadding="5">
            <thead>
            <tr>
                <th colspan="2">Account Information </th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Photo </td>
                    <td><img src="blob" width="150" height="150"></td>
                </tr>
                <tr>
                    <td>Name </td>
                    <td>${result.firstName}</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td> ${result.email} </td>
                </tr>

                <tr>
                    <a href="addFriend?value=${result.idAccount}">Add to Friend</a>
                </tr>
            </tbody>

        </table-->
    </form>
</body>
</html>
