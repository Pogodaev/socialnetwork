<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="${pageContext.request.contextPath}/resources/js/scripts/onTopSearch.js"></script>
</head>
<body>
<nav class="navbar  navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <strong  class="navbar-brand">It's you <a href="account">${uId}</a> </strong>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <form method="post" id="srch" action="searchEmail">
                    <div class="ui-widget" align="down">
                         <input  id="tags" class="ui-autocomplete-input" name="sString"/>
                    </div>
                </form>
            </ul>
        </div>
    </div>
</nav>
<!--script src="https://code.jquery.com/jquery-1.12.4.js"></script-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<input id="query" value="searching" style="display:none">
<input id="lookFor" value="searchEmail?sString=" style="display:none">
</body>
</html>
