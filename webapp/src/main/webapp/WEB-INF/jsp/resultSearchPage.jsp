
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Search Result</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/js/scripts/preseсs.js"></script>
</head>
<body>
<%@include file="search.jsp"%>
<label id="cnt" hidden="hidden">${requestScope.count}</label>
<input id="match"  value="${requestScope.pattern}" style="display:none">
<div class="container" >
    <table class="table-sm table-responsive col-md-6 col-md-offset-3">
        <thead class="thead-default">
        <tr>
            <th>#</th>
            <th>First</th>
            <th>Last</th>
            <th>Email</th>
        </tr>
        </thead>

        <tbody id="div1">
        </tbody>
    </table>
    <button id="back" class="col-md-3 col-md-offset-3">Back</button>
    <button id="next" class="col-md-3 col-md-offset-3">Next</button>
    <p id="page" class="col-md-6 col-md-offset-3"></p>
</div>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</body>
</html>
