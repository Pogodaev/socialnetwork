<%@ page import="java.sql.Blob" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<html>
<head>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/account.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script-->
    <script src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Account</title>
</head>
<body>
<%@include file="search.jsp"%>

<form class="form-inline"  style="width: 100%" align="left">
    <div class="form-group" style="width: 25%">
        <ul class="list-group" style="width: 100%">
            <li>
                <a href="listNotices?value=${uId}" class="list-group-item active">Notices <span class="badge">${nCount}</span></a>
            </li>
            <li>
                <a href="userFriends?value=${uId}" class="list-group-item active">Friends <span class="badge">${fCount}</span></a>
            </li>
            <li>
                <a href="#" class="list-group-item ">Groups </a>
            </li>
            <li>
                <a href="logout" class="list-group-item active">Logout </a>
            </li>
            <li>
                <a href="pregisterAccount" class="list-group-item active">Edit</a>
            </li>
            <li class="list-group-item list-group-item-info">
                <strong class="list-group-item-heading">Middle Name</strong>
                <p class="list-group-item-text">${account.middleName}</p>
            </li>
            <li class="list-group-item list-group-item-info">
                <strong class="list-group-item-heading">Email</strong>
                <p class="list-group-item-text">${account.email}</p>
            </li>
            <li class="list-group-item list-group-item-info">
                <strong class="list-group-item-heading">Home</strong>
                <p class="list-group-item-text">${account.homeAddress}</p>
            </li>
            <li class="list-group-item list-group-item-info">
                <strong class="list-group-item-heading">Skype</strong>
                <p class="list-group-item-text">${account.skype}</p>
            </li>
            <li class="list-group-item list-group-item-info">
                <strong class="list-group-item-heading">ICQ</strong>
                <p class="list-group-item-text">${account.icq}</p>
            </li>
            <li class="list-group-item list-group-item-info">
                <strong class="list-group-item-heading">JOB</strong>
                <p class="list-group-item-text">${account.jobAddress}</p>
            </li>
            <li class="list-group-item list-group-item-info">
                <strong class="list-group-item-heading">AddInfo</strong>
                <p class="list-group-item-text">${account.additional}</p>
            </li>
            <li class="list-group-item list-group-item-info">
                <strong class="list-group-item-heading">BirthDate</strong>
                <p class="list-group-item-text">${account.dateBirth}</p>
            </li>
            <li class="list-group-item list-group-item-info">
                <strong class="list-group-item-heading">Phones</strong>
                <c:forEach var="ph" items="${phones}">
                    <p class="list-group-item-text">${ph}</p>
                </c:forEach>
            </li>

        </ul>
    </div>
    <div id="rightP"  class="form-group align-top" >
        <div class="list-group" >
            <div class="list-group-item" style="border-color: transparent">
                <img src="blob" class="img" width="180" height="240"/>
            </div>
            <div class="list-group-item" style="border-color: transparent; ">
                <Strong>${account.firstName} ${account.lastName} </Strong>
            </div>

            <div class="container list-group-item" style="border-color:transparent;" >
                <div class="row">
                    <div class="col-md-4" style="width:54%; height:60%">
                        <!-- Carousel
                        ================================================== -->
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                <li data-target="#myCarousel" data-slide-to="1"></li>
                                <li data-target="#myCarousel" data-slide-to="2"></li>
                            </ol>

                            <div class="carousel-inner">
                                <div class="item active">
                                    <a href="#"><img class="thumbnail" src="<%=request.getContextPath()%>/resources/img/markothLight.jpeg" width="600" height="400" alt="Slide1"></a>
                                    <div class="caption">
                                        <h4>Markotkh Ultra Trail</h4>
                                        <p>Soon will be enable registration for new Markotkh Ultra Trail running competition. From 17 april will be available 2 distance: 30km and 80 km</p>
                                    </div>
                                </div>
                                <div class="item">
                                    <a href="#"><img class="thumbnail" src="<%=request.getContextPath()%>/resources/img/markoth.jpeg" width="600" height="400" alt="Slide2"></a>
                                    <div class="caption">
                                        <h4>Results of Competition</h4>
                                        <p>On the itra.org you can choose your family and distance</p>
                                    </div>
                                </div>
                                <div class="item">
                                    <a href="#"><img class="thumbnail" src="<%=request.getContextPath()%>/resources/img/phone.jpg" width="600" height="400" alt="Slide3"></a>
                                    <div class="caption">
                                        <h4>Winter running</h4>
                                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. </p>
                                    </div>
                                </div>
                            </div>

                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div><!-- End Carousel -->
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>
</body>
</html>
