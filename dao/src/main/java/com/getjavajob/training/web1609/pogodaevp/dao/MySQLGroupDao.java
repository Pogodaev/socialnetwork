package com.getjavajob.training.web1609.pogodaevp.dao;

import com.getjavajob.training.web1609.pogodaevp.common.Group;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Component(value = "groupDao")
public class MySQLGroupDao extends AbstractJDBCDao<Group, Integer> {


    public MySQLGroupDao() {
        super();
    }

    @Override
    public Class<Group> getClazz() {
        return Group.class;
    }

    private class PersistGroup extends Group {
        public void setId(int id) {
            super.setIdGroup(id);
        }
    }

    @PersistenceContext
    private EntityManager em;


    @Override
    public Group create(Group m) throws DAOException {
        return persist(m);
    }


//    public List<Group> listOfAccountOwnGroups(Account a) {
//        List<Group> result = null;
//        String sql = getListOfAccountGroups();
//        Object[] ob = new Object[] {a.getId()};
//        result = getListQueryWithArgs(sql, ob);
//        return result;
//    }
}
