package com.getjavajob.training.web1609.pogodaevp.dao;

import com.getjavajob.training.web1609.pogodaevp.common.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.LinkedList;
import java.util.List;

@Repository(value = "phonesDao")
public class MySQLPhonesDao extends AbstractJDBCDao<PhoneNumber, Integer> {

    private static final Logger logger = LoggerFactory.getLogger(MySQLPhonesDao.class);

    public MySQLPhonesDao() {
        super();
    }

    @Override
    public Class<PhoneNumber> getClazz() {
        return PhoneNumber.class;
    }

    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;

    @Override
    public PhoneNumber create(PhoneNumber ob) throws DAOException {
        return persist(ob);
    }

    public List<PhoneNumber> getListByIdAccount(Integer id) {
        logger.info("getListByIdAccount just entred");
        List<PhoneNumber> list = new LinkedList<>();
        CriteriaBuilder qb = em.getCriteriaBuilder();
        CriteriaQuery<PhoneNumber> query = qb.createQuery(PhoneNumber.class);
        Root<PhoneNumber> from = query.from(PhoneNumber.class);
        query.select(from);
        query.where(qb.equal(from.get("account"), id));
        TypedQuery<PhoneNumber> typedQuery = em.createQuery(query);
        list = typedQuery.getResultList();
        return list;
    }
}
