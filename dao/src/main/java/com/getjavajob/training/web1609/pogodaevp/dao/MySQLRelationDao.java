package com.getjavajob.training.web1609.pogodaevp.dao;

import com.getjavajob.training.web1609.pogodaevp.common.Account;
import com.getjavajob.training.web1609.pogodaevp.common.Relation;
import com.getjavajob.training.web1609.pogodaevp.common.State;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Repository(value = "relationDao")
public class MySQLRelationDao extends AbstractJDBCDao<Relation, Integer> {

    private static final Logger logger = LoggerFactory.getLogger(MySQLRelationDao.class);

    @Override
    public Relation create(Relation rr) throws DAOException {
        return persist(rr);
    }

    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;

    @Override
    public Class<Relation> getClazz() {
        return Relation.class;
    }

    // done
    public Long getFriendsNumber(Integer first) {
        Account account = em.find(Account.class, first);
        long count = 0l;
        Set<Relation> set1 = account.getMyrelations();
        Set<Relation> set2 = account.getRelations();

        set1.addAll(set2);
        for (Relation r : set1) {
            if (r.getState() == State.accept) {
                count++;
            }
        }
        return count;
    }

    //done
    public long getNoticeCount(Integer second) {
        Account account = em.find(Account.class, second);
        long count = 0l;
        Set<Relation> set1 = account.getMyrelations();

        for (Relation r : set1) {
            if (r.getState() == State.wait) {
                count++;
            }
        }

        return count;
    }

    //done
    public List<Account> getUserNotices(Integer first) {
        List<Account> result = new LinkedList<>();
        Account account = em.find(Account.class, first);

        Set<Relation> set1 = account.getMyrelations();

        for (Relation r : set1) {
            if (r.getState() == State.wait) {
                result.add(r.getAccount1());
            }
        }

        return result;
    }

    //done
    public List<Account> getUserFriends(Integer first) {
        List<Account> result = new LinkedList<>();
        Account account = em.find(Account.class, first);
        Set<Relation> set1 = account.getMyrelations();
        logger.debug("in getUserFriends first Set size " + set1.size());
        Set<Relation> set2 = account.getRelations();
        logger.debug("in getUserFriends second Set size " + set1.size());

        for (Relation r : set1) {
            if (r.getState() == State.accept && r.getAccount1() != account) {
                result.add(r.getAccount1());
            }
        }
        String string = "";
        for (Account a : result) {
            string = string.concat(a.getFirstName() + " ");
        }
        logger.debug("in getUserFriends first Set myrerations " + string);


        for (Relation r : set2) {
            if (r.getState() == State.accept && r.getAccount2() != account) {
                result.add(r.getAccount2());
            }
        }
        string = "";
        for (Account a : result) {
            string = string.concat(a.getFirstName() + " ");
        }
        logger.debug("in getUserFriends second Set rerations " + string);

        return result;
    }


    //Insert into Social.Relation(idFirst, idSecond, state) values (?, ?, 'wait') on DUPLICATE KEY Update state = 'wait';
    public void sendNoticeQuery(Integer first, Integer second) throws DAOException {
        Account account1 = em.find(Account.class, first);
        Account account2 = em.find(Account.class, second);
        Set<Relation> set1 = account1.getMyrelations();

        int id = 0;
        for (Relation r : set1) {
            if (r.getAccount1().getIdAccount() == first && r.getAccount2().getIdAccount() == second) {
                id = r.getIdRelation();
            }
        }

        if (id != 0) {
            Relation relation = em.find(Relation.class, id);
            relation.setState(State.wait);
            update(relation);
        } else {
            Relation relation = new Relation();
            relation.setAccount1(account1);
            relation.setAccount2(account2);
            relation.setState(State.wait);
            create(relation);
        }

    }

    //done
    public void getAcceptNotice(Integer first, Integer second) throws DAOException {
        Account account = em.find(Account.class, first);
        Set<Relation> set1 = account.getRelations();

        int id = 0;
        Relation res = new Relation();
        for (Relation r : set1) {
            if (r.getState() == State.wait) {
                res = r;
                break;
            }
        }

        res.setState(State.accept);
        update(res);
    }

    public void getRejectNotice(Integer first, Integer second) throws DAOException {
        Account account = em.find(Account.class, first);
        Set<Relation> set1 = account.getRelations();

        int id = 0;
        for (Relation r : set1) {
            if (r.getState() == State.wait) {
                id = r.getIdRelation();
            }
        }

        Relation relation = em.find(Relation.class, id);
        relation.setState(State.rejected);
        update(relation);
    }

    public void getDeleteFriend(Integer first, Integer second) throws DAOException {
        Account account = em.find(Account.class, first);
        Set<Relation> set1 = account.getRelations();
        Set<Relation> set2 = account.getMyrelations();

        int id = 0;
        for (Relation r : set1) {
            if (r.getState() == State.accept && r.getAccount2().getIdAccount() == second) {
                id = r.getIdRelation();
            }
        }

        for (Relation r : set2) {
            if (r.getState() == State.accept && r.getAccount1().getIdAccount() == second) {
                id = r.getIdRelation();
            }
        }

        Relation relation = em.find(Relation.class, id);
        relation.setState(State.deleted);
        update(relation);
    }
}
