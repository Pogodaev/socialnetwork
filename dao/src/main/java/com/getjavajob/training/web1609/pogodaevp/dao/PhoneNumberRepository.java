package com.getjavajob.training.web1609.pogodaevp.dao;

import com.getjavajob.training.web1609.pogodaevp.common.Account;
import com.getjavajob.training.web1609.pogodaevp.common.PhoneNumber;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PhoneNumberRepository extends CrudRepository<PhoneNumber, Account> {

    List<PhoneNumber> findByAccount(Account idAccountVal);
}
