package com.getjavajob.training.web1609.pogodaevp.dao;

import com.getjavajob.training.web1609.pogodaevp.common.Identified;

import java.io.Serializable;
import java.util.List;


public interface GenericDao<T extends Identified<PK>, PK extends Serializable> {
    /**
     * Создает новую запись и соответствующий ей объект
     */
    T create(T ob) throws DAOException;

    /**
     * Создает новую запись, соответствующую объекту object
     */
    T persist(T object) throws DAOException;


    /**
     * Сохраняет состояние объекта group в базе данных
     */
    void update(T object) throws DAOException;

    /**
     * Удаляет запись об объекте из базы данных
     */
    void delete(T object) throws DAOException;

    /**
     * Возвращает список объектов соответствующих всем записям в базе данных
     */
    List<T> getAll() throws DAOException;

}
