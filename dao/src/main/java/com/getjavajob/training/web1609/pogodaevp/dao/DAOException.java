package com.getjavajob.training.web1609.pogodaevp.dao;

public class DAOException extends Exception {
    public DAOException(Exception e) {
        System.out.println(e.getMessage());
    }

    public DAOException() {
    }

    public DAOException(String message) {
        super(message);
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }
}
