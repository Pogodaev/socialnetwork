package com.getjavajob.training.web1609.pogodaevp.dao;

import com.getjavajob.training.web1609.pogodaevp.common.Account;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.LinkedList;
import java.util.List;


@Repository(value = "accountDao")
public class MySQLAccountDao extends AbstractJDBCDao<Account, Integer> {

    public MySQLAccountDao() {
        super();
    }

    @Override
    public Class<Account> getClazz() {
        return Account.class;
    }

    @PersistenceContext
    private EntityManager em;

    @Override
    public Account create(Account a) throws DAOException {
        return persist(a);
    }

    /**
     * Login
     */
    public Account searchByEmailAndPassword(String email, String password) throws DAOException {

        List<Account> list = new LinkedList<>();

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Account> criteriaQuery = cb.createQuery(Account.class);
        Root<Account> from = criteriaQuery.from(Account.class);
        ParameterExpression<Integer> p = cb.parameter(Integer.class);

        Predicate emp = cb.equal(from.get("email"), email);
        Predicate psp = cb.equal(from.get("password"), password);
        CriteriaQuery<Account> search = criteriaQuery.select(from).where(cb.and(emp, psp));
        TypedQuery<Account> typedQuery = em.createQuery(search);
        Account resultlist = typedQuery.getSingleResult();
        return resultlist;

    }

    /**
     * Chose from list
     */
    public Account searchByEmail(String email) throws DAOException {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Account> criteriaQuery = cb.createQuery(Account.class);
        Root<Account> from = criteriaQuery.from(Account.class);
        ParameterExpression<Integer> p = cb.parameter(Integer.class);

        CriteriaQuery<Account> search = criteriaQuery.select(from).where(cb.equal(from.get("email"), email));
        TypedQuery<Account> typedQuery = em.createQuery(search);
        Account resultlist = typedQuery.getSingleResult();
        return resultlist;
    }

    public Long getLikeCount(String patt) {
        CriteriaBuilder qb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = qb.createQuery(Long.class);
        Root<Account> from = cq.from(Account.class);
        cq.select(qb.countDistinct(from));
        Predicate emp = qb.like(from.<String>get("firstName"), patt + "%");
        Predicate psp = qb.like(from.<String>get("lastName"), patt + "%");
        cq.where(qb.or(emp, psp));
        long l = em.createQuery(cq).getSingleResult();
        return l;
    }

    public List<Account> getRecordsAccount(Integer start, Integer total, String patt) {
        List<Account> list = new LinkedList<>();
        CriteriaBuilder qb = em.getCriteriaBuilder();
        CriteriaQuery<Account> query = qb.createQuery(Account.class);
        Root<Account> from = query.from(Account.class);
        query.select(from);
        Predicate emp = qb.like(from.<String>get("firstName"), patt + "%");
        Predicate psp = qb.like(from.<String>get("lastName"), patt + "%");
        query.where(qb.or(emp, psp));

        TypedQuery<Account> l = em.createQuery(query).setFirstResult(start).setMaxResults(total);
        list = l.getResultList();
        return list;
    }

}
