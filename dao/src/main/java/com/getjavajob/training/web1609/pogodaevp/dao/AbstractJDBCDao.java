package com.getjavajob.training.web1609.pogodaevp.dao;


import com.getjavajob.training.web1609.pogodaevp.common.Identified;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.List;


public abstract class AbstractJDBCDao<T extends Identified<PK>, PK extends Integer> implements GenericDao<T, PK>, Serializable {

    private Class clazz;

    public AbstractJDBCDao() {
        this.clazz = getClazz();
    }

    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;

    public abstract Class<T> getClazz();

    @Override
    public List<T> getAll() throws DAOException {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);
        Root<T> from = criteriaQuery.from(clazz);

        CriteriaQuery<T> select = criteriaQuery.select(from);
        TypedQuery<T> typedQuery = em.createQuery(select);
        List<T> resultlist = typedQuery.getResultList();
        return resultlist;
    }

    @Override
    public T persist(T object) throws DAOException {
        em.persist(object);
        em.flush();
        return object;
    }

    @Override
    public void update(T object) throws DAOException {
        em.merge(object);
    }

    @Override
    public void delete(T object) throws DAOException {
        T delObject = (T) em.find(clazz, object.getId());
        em.remove(delObject);
        em.flush();
    }
}

