package com.getjavajob.training.web1609.pogodaevp.dao;

import com.getjavajob.training.web1609.pogodaevp.common.Account;
import com.getjavajob.training.web1609.pogodaevp.common.Group;
import org.h2.tools.RunScript;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@org.springframework.transaction.annotation.Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:view-context.xml", "classpath:dao-context-override.xml"})
public class GroupDaoTest {

    @Autowired
    private MySQLGroupDao groupDao;
    @Autowired
    private DataSource dataSource;

    @Before
    public void setUp() throws ClassNotFoundException, SQLException, IOException {
        try (InputStream in = getClass().getClassLoader().getResourceAsStream("CreateAccountTable.sql");
             Reader bufferedReader = new BufferedReader(new InputStreamReader(in));
             Connection connection1 = dataSource.getConnection()) {
             RunScript.execute(connection1, bufferedReader);
        }
    }

    @Test
    public void testCreate() throws DAOException {
        Account account = new Account();
        account.setId(4);

        Group g = new Group();
        g.setAccount(account);
        g.setName("newup");
        g.setIdOwner(1);
        Group gr = groupDao.create(g);
        Assert.assertNotNull(gr);
        for (Group grs : groupDao.getAll()) {
            System.out.println(grs);
        }
    }

    @Test
    public void testUpdate() throws DAOException, SQLException, IOException, ClassNotFoundException {
        Account account = new Account();
        account.setId(1);
        Group g = new Group();

        g.setAccount(account);
        g.setName("new");
        g.setIdGroup(1);

        groupDao.update(g);
        List<Group> list = groupDao.getAll();
        Assert.assertNotNull(list);
        Assert.assertEquals("new", list.get(0).getName());
    }

    @Test
    public void testDelete() throws DAOException, SQLException, IOException, ClassNotFoundException {
        Account account = new Account();
        account.setId(1);
        Group g = new Group();
        g.setAccount(account);
        g.setIdGroup(1);

        List<Group> list = groupDao.getAll();
        int k = list.size();

        groupDao.delete(g);
        Assert.assertEquals(--k, list.size() - 1);
    }
}
