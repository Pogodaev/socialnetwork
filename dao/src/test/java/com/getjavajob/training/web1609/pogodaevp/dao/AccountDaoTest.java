package com.getjavajob.training.web1609.pogodaevp.dao;

import com.getjavajob.training.web1609.pogodaevp.common.Account;
import org.h2.tools.RunScript;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@org.springframework.transaction.annotation.Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:view-context.xml", "classpath:dao-context-override.xml"})
public class AccountDaoTest {

    @Autowired
    private MySQLAccountDao accountDao;
    @Autowired
    private DataSource dataSource;

    @PersistenceContext
    private EntityManager em;

    @Before
    public void setUp() throws ClassNotFoundException, SQLException, IOException {
        try (InputStream in = getClass().getClassLoader().getResourceAsStream("CreateAccountTable.sql");
             Reader bufferedReader = new BufferedReader(new InputStreamReader(in));
             Connection connection1 = dataSource.getConnection()) {
                RunScript.execute(connection1, bufferedReader);
        }
    }


    @Test
    public void testCreate() throws DAOException, SQLException, IOException {
        Account ac = new Account();
        ac.setEmail("test@ya.ru");

        Account a = accountDao.create(ac);
        Assert.assertNotNull(a);

        List<Account> list = accountDao.getAll();

        for (Account ak : list) {
            System.out.println(ak);
        }
        Assert.assertEquals("test@ya.ru", a.getEmail());
        Assert.assertEquals(4, list.size());
    }

    @Test
    public void testGetAll() throws DAOException, SQLException, IOException {
        List<Account> list = accountDao.getAll();
        for (Account ak : list) {
            System.out.println(ak);
        }
        Assert.assertNotNull(list);
        Assert.assertEquals(1, list.get(0).getId().intValue());
        Assert.assertEquals("email@ya.ru", list.get(0).getEmail());
    }

    @Test
    public void testDelete() throws  DAOException, SQLException, IOException, ClassNotFoundException {
        Account ac = new Account();

        List<Account> list = accountDao.getAll();
        ac = list.get(0);
        int k = list.size();

        accountDao.delete(ac);
        List<Account> res = accountDao.getAll();
        for (Account a : res) {
            System.out.println(a);
        }
        Assert.assertEquals(--k, res.size());
        Assert.assertEquals(Integer.valueOf(2), res.get(0).getId());
    }

    @Test
    public void testUpdate() throws DAOException, SQLException, IOException {
        Account a = new Account();
        List<Account> ll = accountDao.getAll();
        for (Account ac : ll) {
            System.out.println(ac);
        }
        a = ll.get(1);
        a.setLastName("update");
        a.setEmail("email");


        accountDao.update(a);
        List<Account> l = accountDao.getAll();
        for (Account ac : l) {
            System.out.println(ac);
        }
        Assert.assertNotNull(l.get(0));
        Assert.assertEquals("update", l.get(1).getLastName());
    }

}
