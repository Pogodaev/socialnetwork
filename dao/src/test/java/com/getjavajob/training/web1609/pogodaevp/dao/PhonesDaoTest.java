package com.getjavajob.training.web1609.pogodaevp.dao;

import com.getjavajob.training.web1609.pogodaevp.common.Account;
import com.getjavajob.training.web1609.pogodaevp.common.PhoneNumber;
import org.h2.tools.RunScript;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.io.*;
import java.sql.Connection;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:view-context.xml", "classpath:dao-context-override.xml"})
public class PhonesDaoTest {
    @Autowired
    private MySQLPhonesDao phonesDao;
    @Autowired
    private MySQLAccountDao accountDao;
    @Autowired
    private DataSource dataSource;

    @PersistenceContext
    private EntityManager em;
    @Before
    public void setUp() throws ClassNotFoundException, SQLException, IOException {
        try (InputStream in = getClass().getClassLoader().getResourceAsStream("CreateAccountTable.sql");
             Reader bufferedReader = new BufferedReader(new InputStreamReader(in));
             Connection connection1 = dataSource.getConnection()) {
            RunScript.execute(connection1, bufferedReader);
        }
    }

    @Test
    public void testCreate() throws DAOException {

        Account a = new Account();
        a.setFirstName("first");
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setAccount(a);
        phoneNumber.setPhons("+742098098");
        a.addPhoneNumber(phoneNumber);
        Account account = accountDao.create(a);
        System.out.println(account);

        System.out.println(account.getPhones());
        String number = (new LinkedHashSet<PhoneNumber>(account.getPhones()){}).toArray(new PhoneNumber[]{})[0].getPhons();
        Assert.assertNotNull(new LinkedHashSet<PhoneNumber>(account.getPhones()){});
        Assert.assertEquals("+742098098", number);
    }

    @Test
    public void testDelete() throws DAOException, SQLException, IOException, ClassNotFoundException {
        Account a = new Account();
        a.setFirstName("first");
        PhoneNumber p1 = new PhoneNumber();
        p1.setAccount(a);
        p1.setPhons("+742098098");
        a.addPhoneNumber(p1);
        PhoneNumber p2 = new PhoneNumber();
        p2.setAccount(a);
        p2.setPhons("+711111");
        a.addPhoneNumber(p2);
        Account account = accountDao.create(a);
        System.out.println(account);

        account.removePhoneNumber(p2);
        accountDao.update(account);
        Assert.assertEquals(1, accountDao.getAll().get(3).getPhones().size());

    }

//    @Test
    public void testGetAll() throws DAOException {
        List<PhoneNumber> list = phonesDao.getAll();
        Assert.assertNotNull(list);
        Assert.assertEquals("+79163672477", list.get(0).getPhons());
    }

    @Test
    public void testUpdate() throws DAOException {
        Set<PhoneNumber> set = new HashSet<>();
        Account account = new Account();
        account.setFirstName("first");

        PhoneNumber p = new PhoneNumber();
        p.setAccount(account);
        p.setPhons("+79160000001");
        set.add(p);

        account.setPhones(set);
        Account s = accountDao.create(account);
        for (Account account1 : accountDao.getAll()) {
            System.out.println("\n");
            System.out.println(account1);
            System.out.println(account1.getPhones());
            System.out.println("\n");
        }
//        p.setAccountId(1);

        set.clear();
        PhoneNumber ph = new PhoneNumber();
        ph.setAccount(account);
        ph.setPhons("+7916333333");
        set.add(ph);
        account.setPhones(set);
        System.out.println(p);
        accountDao.update(account);
        for (Account account1 : accountDao.getAll()) {
            System.out.println("\n");
            System.out.println(account1);
            System.out.println(account1.getPhones());
            System.out.println("\n");
        }
//        phonesDao.update(p);
        for (PhoneNumber phs : phonesDao.getAll()) {
            System.out.println(phs);
        }
        Assert.assertEquals("+7916333333",  accountDao.getAll().get(3).getPhones().toArray(new PhoneNumber[]{})[0].getPhons());
    }

}
