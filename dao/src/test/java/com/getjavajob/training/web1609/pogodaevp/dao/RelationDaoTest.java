package com.getjavajob.training.web1609.pogodaevp.dao;

import com.getjavajob.training.web1609.pogodaevp.common.Account;
import com.getjavajob.training.web1609.pogodaevp.common.Relation;
import org.h2.tools.RunScript;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import static org.junit.Assert.assertEquals;

@org.springframework.transaction.annotation.Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:view-context.xml", "classpath:dao-context-override.xml"})
public class RelationDaoTest {

    @Autowired
    private MySQLRelationDao relationDao;
    @Autowired
    private DataSource dataSource;


   @Before
    public void setUp() throws ClassNotFoundException, SQLException, IOException {
       try (InputStream in = getClass().getClassLoader().getResourceAsStream("CreateAccountTable.sql");
            Reader bufferedReader = new BufferedReader(new InputStreamReader(in));
            Connection connection1 = dataSource.getConnection()) {
           RunScript.execute(connection1, bufferedReader);
       }
    }

    @Test
    public  void addToFriends() throws DAOException, SQLException {
        Account ac1 = new Account();
        ac1.setIdAccount(1);
        Account ac2 = new Account();
        ac2.setIdAccount(2);

        Relation start = new Relation();
        start.setAccount1(ac1);

        start.setAccount2(ac2);

        Relation t = relationDao.create(start);

        assertEquals(3, relationDao.getAll().size());
        assertEquals(Integer.valueOf(2), relationDao.getAll().get(1).getId());
    }

    @Test
    public void deleteFromFriends() throws DAOException, SQLException {
        Account ac1 = new Account();
        ac1.setId(1);
        Account ac2 = new Account();
        ac1.setId(3);

        Relation start = new Relation();
        start.setIdRelation(1);
        start.setAccount1(ac2);
        start.setAccount2(ac1);

        for (Relation rl : relationDao.getAll()) {
            System.out.println(rl);
        }
        relationDao.delete(start);
        assertEquals(1, relationDao.getAll().size());
        assertEquals(new Integer(2), relationDao.getAll().get(0).getAccount1().getIdAccount());
    }

}
