DROP SCHEMA IF EXISTS Social;
CREATE SCHEMA IF NOT EXISTS Social;
USE Social;
CREATE TABLE IF NOT EXISTS Social.`ACCOUNT` (
  `idAccount` int(11) NOT NULL AUTO_INCREMENT,
  `lastName` varchar(45) DEFAULT NULL,
  `firstName` varchar(45) DEFAULT NULL,
  `middleName` varchar(45) DEFAULT NULL,
  `mobileNumber` varchar(45) DEFAULT NULL,
  `mobileNumber2` varchar(45) DEFAULT NULL,
  `homeNumber` varchar(45) DEFAULT NULL,
  `homeAddress` varchar(45) DEFAULT NULL,
  `jobAddress` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `icq` varchar(45) DEFAULT NULL,
  `skype` varchar(45) DEFAULT NULL,
  `additional` varchar(45) DEFAULT NULL,
  `dateBirth` date DEFAULT NULL,
  `regDate` date DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `img` blob,
  PRIMARY KEY (`idAccount`));
insert into Social.Account(
  `lastName`,
  `firstName`,
  `middleName`,
  `homeNumber`,
  `homeAddress`,
  `jobAddress`,
  `email`,
  `icq`,
  `skype`,
  `additional`,
  `dateBirth`,
  `regDate`,
  `password`,
  `img`)  values( 'Hello', 'olen', 'Mat', '+789999999', 'Kiev', 'Kiss', 'email@ya.ru', 'icq2', 'skype3', 'additional info 33', '1990-03-01', '1980-11-01', '123', '');
insert into Social.Account(
  `lastName`,
  `firstName`,
  `middleName`,
  `homeNumber`,
  `homeAddress`,
  `jobAddress`,
  `email`,
  `icq`,
  `skype`,
  `additional`,
  `dateBirth`,
  `regDate`,
  `password`,
  `img`)  values( 'Vova', 'Ovechkin', 'Mat', '+789999999', 'Kiev', 'Kiss', 'email@gmail.ru', 'icq2', 'skype3', 'additional info 33', '1990-03-01', '1980-11-01', '123', '');
insert into Social.Account(
  `lastName`,
  `firstName`,
  `middleName`,
  `homeNumber`,
  `homeAddress`,
  `jobAddress`,
  `email`,
  `icq`,
  `skype`,
  `additional`,
  `dateBirth`,
  `regDate`,
  `password`,
  `img`)  values( 'Zina', 'Diger', 'Sat', '+78111111',  'Irk', 'last', 'email@rambler.ru', 'icq3', 'skype4', 'additional info 333', '1990-01-01', '1970-03-31', '12367','');
CREATE TABLE IF NOT EXISTS Social.`GROUPS`
 (`idGroup` INT(11) NOT NULL AUTO_INCREMENT ,
   `name` varchar(45)  NULL,
  `image` blob DEFAULT NULL,
   `idAccount` INT (11)  NULL,
   PRIMARY KEY (`idGroup`),
  FOREIGN KEY (`idAccount`) REFERENCES `Account` (`idAccount`) ON DELETE CASCADE);
insert into Social.Groups(name, idAccount) values( 'Public', 1);
insert into Social.Groups(name, idAccount) values( 'Esquire', 1);