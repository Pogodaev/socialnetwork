DROP TABLE IF EXISTS ACCOUNT;
CREATE TABLE  ACCOUNT (
  `idAccount` int(11) NOT NULL AUTO_INCREMENT,
  `lastName` varchar(45) DEFAULT NULL,
  `firstName` varchar(45) DEFAULT NULL,
  `middleName` varchar(45) DEFAULT NULL,
  `mobileNumber` varchar(45) DEFAULT NULL,
  `mobileNumber2` varchar(45) DEFAULT NULL,
  `homeNumber` varchar(45) DEFAULT NULL,
  `homeAddress` varchar(45) DEFAULT NULL,
  `jobAddress` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `icq` varchar(45) DEFAULT NULL,
  `skype` varchar(45) DEFAULT NULL,
  `additional` varchar(45) DEFAULT NULL,
  `dateBirth` date DEFAULT NULL,
  `regDate` date DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `img` mediumblob,
  PRIMARY KEY (`idAccount`));
insert into Account(
  `lastName`,
  `firstName`,
  `middleName`,
  `homeNumber`,
  `homeAddress`,
  `jobAddress`,
  `email`,
  `icq`,
  `skype`,
  `additional`,
  `dateBirth`,
  `regDate`,
  `password`,
  `img`)  values( 'Hello', 'olen', 'Mat', '+789999999', 'Kiev', 'Kiss', 'email@ya.ru', 'icq2', 'skype3', 'additional info 33', '1990-03-01', '1980-11-01', '123', '');
insert into Account(
                           `lastName`,
                           `firstName`,
                           `middleName`,
                           `homeNumber`,
                           `homeAddress`,
                           `jobAddress`,
                           `email`,
                           `icq`,
                           `skype`,
                           `additional`,
                           `dateBirth`,
                           `regDate`,
                           `password`,
                           `img`)  values( 'Vova', 'Ovechkin', 'Mat', '+789999999', 'Kiev', 'Kiss', 'email@gmail.ru', 'icq2', 'skype3', 'additional info 33', '1990-03-01', '1980-11-01', '123', '');
insert into Account(
                           `lastName`,
                           `firstName`,
                           `middleName`,
                           `homeNumber`,
                           `homeAddress`,
                           `jobAddress`,
                           `email`,
                           `icq`,
                           `skype`,
                           `additional`,
                           `dateBirth`,
                           `regDate`,
                           `password`,
                           `img`)  values( 'Zina', 'Diger', 'Sat', '+78111111',  'Irk', 'last', 'email@rambler.ru', 'icq3', 'skype4', 'additional info 333', '1990-01-01', '1970-03-31', '12367','');

DROP TABLE IF EXISTS GROUPS;
CREATE TABLE GROUPS
(`idGroup` INT(11) NOT NULL AUTO_INCREMENT ,
 `name` varchar(45)  NULL,
 `image` blob DEFAULT NULL,
 `idAccount` INT (11)  NULL,
  PRIMARY KEY (`idGroup`),
  FOREIGN KEY (`idAccount`) REFERENCES `Account` (`idAccount`) ON DELETE CASCADE);
insert into Groups(name, idAccount) values( 'Public', 1);
insert into Groups(name, idAccount) values( 'Esquire', 1);

DROP TABLE IF EXISTS Phones;
CREATE TABLE Phones (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idAccount` int(11) NOT NULL,
  `phons` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`idAccount`) REFERENCES `Account` (`idAccount`) ON DELETE CASCADE
);
INSERT INTO Phones(idAccount, phons)  VALUES ( 1, '+79163672477');
INSERT INTO Phones(idAccount, phons)  VALUES ( 2, '+79039763397');

DROP TABLE IF EXISTS Relation;

CREATE DOMAIN IF NOT EXISTS enum as VARCHAR(255);

CREATE TABLE IF NOT EXISTS Relation (
  `idRelation` int(11) NOT NULL AUTO_INCREMENT,
  `idFirst` int(11) NOT NULL,
  `idSecond` int(11) NOT NULL,
  `state` enum DEFAULT NULL,
  PRIMARY KEY (`idRelation`),
  FOREIGN KEY (`idFirst`) REFERENCES `Account` (`idAccount`) ON DELETE CASCADE);
insert into  Relation(idFirst, idSecond) values(1, 3);
insert into  Relation(idFirst, idSecond) values(2, 3);