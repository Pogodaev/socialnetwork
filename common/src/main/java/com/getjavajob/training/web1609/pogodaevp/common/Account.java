package com.getjavajob.training.web1609.pogodaevp.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Blob;
import java.sql.Date;
import java.util.*;

@Entity
@Table(name = "Account")
public class Account implements Identified<Integer>, Serializable, UserDetails {

    private Integer idAccount;

    private String lastName;

    private String firstName;

    private String middleName;

    private String[] phons; //add dao done

    private String homeNumber;

    private String homeAddress;

    private String jobAddress;

    private String email;

    private String icq;

    private String skype;

    private String additional;

    private Date dateBirth;

    private Date regDate;

    private String password;
    @JsonIgnore
    private Blob img;
    private List<Group> groups;
    private Set<PhoneNumber> phones = new LinkedHashSet<>();
    private Set<Relation> relations;
    private Set<Relation> myrelations;


    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    @OneToMany(mappedBy="account")
    public List<Group> getGroups() {
        return groups;
    }

    @OneToMany(mappedBy = "account", cascade = {CascadeType.PERSIST,  CascadeType.REMOVE, CascadeType.MERGE, CascadeType.DETACH})
    public Set<PhoneNumber> getPhones() {
        return this.phones;
    }

    @OneToMany(mappedBy = "account1")
    public Set<Relation> getRelations() {
        return relations;
    }

    @OneToMany(mappedBy = "account2" )
    public Set<Relation> getMyrelations() {
        return myrelations;
    }

    public void setMyrelations(Set<Relation> myrelations) {
        this.myrelations = myrelations;
    }

    public void setRelations(Set<Relation> relations) {
        this.relations = relations;
    }

    public void setPhones(Set<PhoneNumber> phones) {
        this.phones = phones;
    }

    public void addPhoneNumber(PhoneNumber phoneNumber) {
        phones.add(phoneNumber);
        phoneNumber.setAccount(this);
    }

    public void removePhoneNumber(PhoneNumber phoneNumber) {
        phones.remove(phoneNumber);
        phoneNumber.setAccount(null);
    }

    public Account() {

    }

    public void setIdAccount(Integer idAccount) {
        this.idAccount = idAccount;
    }

    @Transient
    public String[] getPhons() {
        return phons;
    }

    public void setPhons(String[] phons) {
        this.phons = phons;
    }

    public Blob getImg() {
        return img;
    }

    public void setImg(Blob img) {
        this.img = img;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }
    @Transient
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority("ROLE_" + this.firstName));
    }

    public String getPassword() {
        return password;
    }
    @Transient
    @Override
    public String getUsername() {
        return email;
    }
    @Transient
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    @Transient
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
@Transient
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
@Transient
    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(Date dateBirth) {
        this.dateBirth = dateBirth;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getIdAccount() {
        return idAccount;
    }

    public void setId(int idAccount) {
        this.idAccount = idAccount;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getJobAddress() {
        return jobAddress;
    }

    public void setJobAddress(String jobAddress) {
        this.jobAddress = jobAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getAdditional() {
        return additional;
    }

    public void setAdditional(String additional) {
        this.additional = additional;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + idAccount +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", homeAddress='" + homeAddress + '\'' +
                ", jobAddress='" + jobAddress + '\'' +
                ", email='" + email + '\'' +
                ", icq='" + icq + '\'' +
                ", skype='" + skype + '\'' +
                ", additional='" + additional + '\'' +
                ", dateBirth=" + dateBirth +
                '}';
    }


    @Override
    @Transient
    public Integer getId() {
        return idAccount;
    }
}