package com.getjavajob.training.web1609.pogodaevp.common;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Relation")
public class Relation implements Identified<Integer> {

    private Integer idRelation;
    private State state;
    private Account account1;
    private Account account2;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getIdRelation() {
        return idRelation;
    }

    @Enumerated(EnumType.STRING)
    public State getState() {
        return state;
    }

    @ManyToOne
    @JoinColumn(name = "idFirst", referencedColumnName = "idAccount",  nullable = false)
    public Account getAccount1() {
        return account1;
    }

    public void setAccount1(Account account1) {
        this.account1 = account1;
    }

    @ManyToOne
    @JoinColumn(name = "idSecond", referencedColumnName = "idAccount")
    public Account getAccount2() {
        return account2;
    }

    public void setAccount2(Account account2) {
        this.account2 = account2;
    }

    @Transient
    public Integer getId() {
        return idRelation;
    }

    public void setIdRelation(Integer idRelation) {
        this.idRelation = idRelation;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void setAccount1Id(Integer accountId) {
        this.account1.setId(accountId);
    }

    public void setAccount2Id(Integer accountId) {
        this.account2.setId(accountId);
    }
    @Override
    public String toString() {
        return "Relation{" +
                "id=" + idRelation +
                ", account1=" + account1 +
                ", account2=" + account2 +
                '}';
    }

}
