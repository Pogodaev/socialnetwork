package com.getjavajob.training.web1609.pogodaevp.common;

import javax.persistence.*;

@Entity
@Table(name = "Phones")
public class PhoneNumber implements Identified<Integer> {

    private Integer id;
    private Account account;
    private String phons;



    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "idAccount", referencedColumnName = "idAccount")
    public Account getAccount() {
        return this.account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public void setAccountId(Integer accountId) {
        this.account.setId(accountId);
    }
    @Column(name = "phons")
    public String getPhons() {
        return phons;
    }

    public void setPhons(String numbers) {
        this.phons = numbers;
    }

    @Override
    public String toString() {
        return "PhoneNumber{" +
                "id=" + id +
                ", accountId=" + account.getId() +
                ", numbers='" + phons + '\'' +
                '}';
    }
}
