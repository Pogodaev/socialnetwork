package com.getjavajob.training.web1609.pogodaevp.common;

import javax.persistence.*;
import java.sql.Blob;
@Entity
@Table(name = "GROUPS")
public class Group implements Identified<Integer> {

    private Integer idGroup;
    private String name;
    private Account account;
    private Blob image;


    public void setIdGroup(Integer idGroup) {
        this.idGroup = idGroup;
    }

    @ManyToOne
    @JoinColumn(name = "idAccount", nullable=false)
    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Blob getImage() {
        return image;
    }

    public void setImage(Blob image) {
        this.image = image;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setIdOwner(int idOwner) {
        account.setId(idOwner);
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + idGroup +
                ", name='" + name + '\'' +
//                ", idOwner=" + idAccou +
                '}';
    }

    @Override
    @Transient
    public Integer getId() {
        return idGroup;
    }
}
