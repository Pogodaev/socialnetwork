package com.getjavajob.training.web1609.pogodaevp.common;


public enum State {
    wait, accept, rejected, deleted
}
