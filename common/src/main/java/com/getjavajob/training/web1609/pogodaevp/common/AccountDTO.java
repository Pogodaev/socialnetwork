package com.getjavajob.training.web1609.pogodaevp.common;

import java.sql.Date;

public class AccountDTO {
    private Integer idAccount;
    private String lastName;
    private String firstName;
    private String middleName;
    private String homeNumber;
    private String homeAddress;
    private String jobAddress;
    private String email;
    private String icq;
    private String skype;
    private String additional;
    private Date dateBirth;
    private Date regDate;
    private String password;

    public AccountDTO(Account account) {
        this.email = account.getEmail();
        this.firstName = account.getFirstName();
        this.lastName = account.getLastName();
        this.idAccount = account.getIdAccount();
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getEmail() {
        return email;
    }
}
