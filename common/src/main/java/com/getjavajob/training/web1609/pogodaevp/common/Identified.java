package com.getjavajob.training.web1609.pogodaevp.common;

import java.io.Serializable;

public interface Identified<PK extends Serializable> {

    /** Возвращает идентификатор объекта */
    PK getId();
}
