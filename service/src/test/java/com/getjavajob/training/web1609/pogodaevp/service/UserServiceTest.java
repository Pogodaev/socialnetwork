package com.getjavajob.training.web1609.pogodaevp.service;

import com.getjavajob.training.web1609.pogodaevp.common.Account;
import com.getjavajob.training.web1609.pogodaevp.common.PhoneNumber;
import com.getjavajob.training.web1609.pogodaevp.dao.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.LinkedList;
import java.util.List;

@org.springframework.transaction.annotation.Transactional
@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    private MySQLPhonesDao daoPhones;

    private MySQLAccountDao daoAccount;

    private UserService service;

    @Before
    public void init() {
        daoAccount = Mockito.mock(MySQLAccountDao.class);
        daoPhones = Mockito.mock(MySQLPhonesDao.class);
        service = new UserService(daoPhones, daoAccount);
    }

//    @Test
    public void testCreate() throws DAOException, ServiceException {
        Account account = new Account();
        account.setId(1);
        account.setLastName("Hello");

        Mockito.when(daoAccount.create(Mockito.any(Account.class))).thenReturn(account);
        Account r = service.createAccount(account, new String[] {" "});

        Assert.assertNotNull(r);
        Assert.assertEquals("Hello", r.getLastName());
    }

//    @Test
    public void testUpdate() throws DAOException, ServiceException {
        Account account = new Account();
        account.setId(1);
        account.setLastName("LastName");
        account.setLastName("New");

        Account r = new Account();
        r.setId(2);
        r.setLastName("New");
        service.updateAccount(r, new String[] {" "});
        Mockito.verify(daoAccount).update(r);
    }

    @Test
    public void testSearchEmPass() throws DAOException, ServiceException {
        Account account = new Account();
        account.setId(1);
        account.setLastName("Hello");
        account.setEmail("email");
        account.setPassword("pass");

        Mockito.when(daoAccount.searchByEmailAndPassword("email", "pass")).thenReturn(account);

        Account result = service.searchByEmailPassword(account.getEmail(), account.getPassword());
        Assert.assertEquals("Hello", result.getLastName());

    }

//    @Test
    public void testPhoneNumbers() throws DAOException, ServiceException {
        List<PhoneNumber> list = new LinkedList<>();
        PhoneNumber ph = new PhoneNumber();
        Account a = new Account();
        a.setId(1);
        ph.setAccount(a);
//        ph.setAccountId(1);
        a.setId(2);
        ph.setPhons("+7809728097");
        PhoneNumber ph2 = new PhoneNumber();
        ph2.setId(2);
//        ph2.setAccountId(2);
        ph2.setPhons("+7809728444");
        list.add(ph);
        list.add(ph2);

        Mockito.when(daoPhones.getAll()).thenReturn(list);
        List<String> l = service.listPhoneNumbers(1);
        Assert.assertNotNull(l);
        Assert.assertEquals(1, l.size());
        Assert.assertEquals("+7809728097", l.get(0));
    }

    @Test
    public void testGetUsers() throws DAOException, ServiceException {
        List<Account> list = new LinkedList<>();
        Account account = new Account();
        account.setId(1);
        account.setLastName("Hello");
        Account account2 = new Account();
        account2.setId(2);
        account2.setLastName("Hello2");
        list.add(account);
        list.add(account2);

        Mockito.when(daoAccount.getAll()).thenReturn(list);
        List<Account> l = service.getAllUsers();
        Assert.assertNotNull(l);
        Assert.assertEquals(2, l.size());
        Assert.assertEquals("Hello", l.get(0).getLastName());
    }
}
