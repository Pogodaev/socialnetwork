package com.getjavajob.training.web1609.pogodaevp.service;


import com.getjavajob.training.web1609.pogodaevp.common.Account;
import com.getjavajob.training.web1609.pogodaevp.dao.DAOException;
import com.getjavajob.training.web1609.pogodaevp.dao.MySQLAccountDao;
import com.getjavajob.training.web1609.pogodaevp.dao.MySQLRelationDao;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import org.mockito.runners.MockitoJUnitRunner;

import java.util.LinkedList;
import java.util.List;

@org.springframework.transaction.annotation.Transactional
@RunWith(MockitoJUnitRunner.class)
public class FriendsServiceTest {

    private MySQLAccountDao daoAccount;

    private MySQLRelationDao relationDao;

    private FriendsService service;

    @Before
    public void init() {
        daoAccount = Mockito.mock(MySQLAccountDao.class);
        relationDao = Mockito.mock(MySQLRelationDao.class);
        service = new FriendsService(daoAccount, relationDao);
    }

    @Test
    public void testSendNotice() throws DAOException {
        Account account = new Account();
        Account account1 = new Account();
        account.setId(1);
        account1.setId(2);

        try {
            service.sendNoticeTo(account.getId(), account1.getId());
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        Mockito.verify(relationDao).sendNoticeQuery(1,2);
    }

    @Test
    public void testGetUserNoticeCount() {
        Account account = new Account();
        account.setId(1);

        service.getUserNoticeCount(account.getId());
        Mockito.verify(relationDao).getNoticeCount(1);
    }

    @Test
    public void testGetUserFriendsCount() {
        Account account = new Account();
        account.setId(1);

        service.getUserFriendsCount(account.getId());
        Mockito.verify(relationDao).getFriendsNumber(1);
    }

    @Test
    public void testGetNoticeFriendsOfUser() {
        Account account = new Account();
        account.setId(123);

        List<Account> list = new LinkedList<Account>();
        Account a = new Account();
        a.setId(1);
        list.add(a);

        Mockito.when(relationDao.getUserNotices(123)).thenReturn(list);

        List<Account> res = service.getNoticeFriendsOfUser(account.getId());
        Assert.assertEquals(1, res.size());
    }

    @Test
    public void testGetUserFriends() {
        Account account = new Account();
        account.setId(1234);

        List<Account> list = new LinkedList<Account>();
        Account a = new Account();
        a.setId(1);
        list.add(a);

        Mockito.when(relationDao.getUserFriends(1234)).thenReturn(list);

        List<Account> res = service.getUserFriends(account.getId());
        Assert.assertEquals(1, res.size());
    }

    @Test
    public void testAcceptFriendship() throws ServiceException, DAOException {
        Account account = new Account();
        account.setId(1234);
        Account account1 = new Account();
        account1.setId(14);

        service.acceptFriendship(account.getId(), account1.getId());
        Mockito.verify(relationDao).getAcceptNotice(account.getId(), account1.getId());
    }

    @Test
    public void testRejectFriendship() throws ServiceException, DAOException {
        Account account = new Account();
        account.setId(124);
        Account account1 = new Account();
        account1.setId(4);

        service.rejectFriendship(account.getId(), account1.getId());
        Mockito.verify(relationDao).getRejectNotice(account.getId(), account1.getId());
    }

    @Test
    public void testDeleteFriendship() throws ServiceException, DAOException {
        Account account = new Account();
        account.setId(124);
        Account account1 = new Account();
        account1.setId(4);

        service.deleteFriend(account.getId(), account1.getId());
        Mockito.verify(relationDao).getDeleteFriend(account.getId(), account1.getId());
    }

}
