package com.getjavajob.training.web1609.pogodaevp.service;

import com.getjavajob.training.web1609.pogodaevp.dao.MySQLAccountDao;
import com.getjavajob.training.web1609.pogodaevp.dao.MySQLGroupDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "groupService")
public class GroupService {

    private MySQLAccountDao accountDao;

    private MySQLGroupDao groupDao;

    @Autowired
    public GroupService(MySQLAccountDao accountDao, MySQLGroupDao groupDao) {
        this.accountDao = accountDao;
        this.groupDao = groupDao;
    }
}
