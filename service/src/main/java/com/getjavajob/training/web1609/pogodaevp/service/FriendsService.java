package com.getjavajob.training.web1609.pogodaevp.service;


import com.getjavajob.training.web1609.pogodaevp.common.Account;
import com.getjavajob.training.web1609.pogodaevp.dao.DAOException;
import com.getjavajob.training.web1609.pogodaevp.dao.MySQLAccountDao;
import com.getjavajob.training.web1609.pogodaevp.dao.MySQLRelationDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service(value = "friendsService")
public class FriendsService {

    private MySQLAccountDao daoAccount;
    private MySQLRelationDao relationDao;

    @Autowired
    public FriendsService(MySQLAccountDao daoAccount, MySQLRelationDao relationDao) {
        this.daoAccount = daoAccount;
        this.relationDao = relationDao;
    }

    public void sendNoticeTo(int first, int second) throws ServiceException {
        try {
            relationDao.sendNoticeQuery(first, second);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public Long getUserNoticeCount(int second) {
        return relationDao.getNoticeCount(second);
    }

    public Long getUserFriendsCount(int first) {
        return relationDao.getFriendsNumber(first);
    }

    public List<Account> getNoticeFriendsOfUser(int first) {
        return relationDao.getUserNotices(first);
    }

    public List<Account> getUserFriends(int first) {
        return relationDao.getUserFriends(first);
    }

    public void acceptFriendship(int first, int second) throws ServiceException {
        try {
            relationDao.getAcceptNotice(first, second);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void rejectFriendship(int first, int second) throws ServiceException {
        try {
            relationDao.getRejectNotice(first, second);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void deleteFriend(int first, int second) throws ServiceException {
        try {
            relationDao.getDeleteFriend(first, second);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

}
