package com.getjavajob.training.web1609.pogodaevp.service;

public class ServiceException extends Exception {
    public ServiceException(Exception e) {
        System.out.println(e.getMessage());
    }

    public ServiceException() {

    }

    public ServiceException(String message) {
        super(message);
    }
}
