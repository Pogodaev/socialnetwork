package com.getjavajob.training.web1609.pogodaevp.service;

import com.getjavajob.training.web1609.pogodaevp.common.Account;
import com.getjavajob.training.web1609.pogodaevp.common.PhoneNumber;
import com.getjavajob.training.web1609.pogodaevp.dao.DAOException;
import com.getjavajob.training.web1609.pogodaevp.dao.MySQLAccountDao;
import com.getjavajob.training.web1609.pogodaevp.dao.MySQLPhonesDao;
import com.getjavajob.training.web1609.pogodaevp.dao.PhoneNumberRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


@Service(value = "userService")
public class UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);
    private MySQLPhonesDao daoPhones;
    private MySQLAccountDao daoAccount;
    private PhoneNumberRepository daoPhonesNew;

    @Autowired
    public UserService(MySQLPhonesDao daoPhones, MySQLAccountDao daoAccount) {
        this.daoPhones = daoPhones;
        this.daoAccount = daoAccount;
    }

  //  @Autowired
    public UserService(PhoneNumberRepository daoPhonesNew, MySQLAccountDao daoAccount) {
        this.daoPhonesNew = daoPhonesNew;
        this.daoAccount = daoAccount;
    }

    public UserService() {

    }

    @Transactional
    public Account createAccount(Account account, String[] phones) throws ServiceException {
        Account res = new Account();
        for (String ph : phones) {
            PhoneNumber phoneNumber = new PhoneNumber();
            phoneNumber.setPhons(ph);
            account.addPhoneNumber(phoneNumber);
        }
        try {
            res = daoAccount.create(account);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return res;
    }

    @Transactional
    public void updateAccount(Account account, String[] phones) throws ServiceException {
        try {
            logger.debug("Service Update Account ");
            List<PhoneNumber> l = daoPhones.getListByIdAccount(account.getId());
            for (PhoneNumber phoneNumber : l ) {
                daoPhones.delete(phoneNumber);
            }

            for (String ph : phones) {
                PhoneNumber phoneNumber = new PhoneNumber();
                phoneNumber.setPhons(ph);
                phoneNumber.setAccount(account);
                account.removePhoneNumber(phoneNumber);
            }
            String[] res = account.getPhons();
            for (String s : res) {
                logger.debug("Update account phone number accPhons" + s + " ");
                PhoneNumber phoneNumber = new PhoneNumber();
                phoneNumber.setPhons(s);
                account.addPhoneNumber(phoneNumber);
            }
            daoAccount.update(account);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public Account searchByEmailPassword(String email, String password) throws ServiceException {
        Account account = null;
        try {
            account = daoAccount.searchByEmailAndPassword(email, password);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return account;
    }

    public List<String> listPhoneNumbers(Integer id) throws ServiceException {
        List<String> res = new LinkedList<>();

        Account account = new Account();
        account.setId(id);
        List<PhoneNumber> result = daoPhones.getListByIdAccount(id);
//        List<PhoneNumber> result = daoPhonesNew.findByAccount(account);
        logger.debug("I'm in listPhoneNumbers em.find getPhones: " + result.toString());
        for (PhoneNumber ph : result) {
            res.add(ph.getPhons());
        }
        return res;
    }

    public List<Account> getAllUsers() throws ServiceException {
        List<Account> accountList = null;
        try {
            accountList = daoAccount.getAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        } finally {
            return accountList;
        }
    }

    public List<Account> searchByEmail(String msg) throws ServiceException {
        List<Account> accounts = null;
        try {
            Account account = daoAccount.searchByEmail(msg);
            if (account != null) {
                accounts = new LinkedList<>();
                accounts.add(account);
            } else {
                account = new Account();
                account.setFirstName("nope");
                account.setEmail("nope");
                account.setLastName("nope");
                accounts.add(account);
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return accounts;
    }

    public Long getCountGuys(String match) {
        return daoAccount.getLikeCount(match);
    }

    public List<Account> getRecords(Integer start, Integer total, String patt) {
        return daoAccount.getRecordsAccount(start, total, patt);
    }
}
