# Yet Another Social Network
 
** Functionality: **
 
+ registration  
+ ajax loading of friends  
+ authentication  
+ ajax search with pagination  
+ display profile  
+ edit profile  
+ upload and download avatar  
+ users export to xml  
+ adding/deleting friends

**Tools:**

JDK 7, Spring 4, JPA 2 / Hibernate 4, jQuery 2, Twitter Bootstrap 3, JUnit 4, Mockito, Maven 3, Git / Bitbucket, Tomcat 7, MySQL, IntelliJIDEA 16.  
 
 



Java Web, December 2016  
_  
**Pogodaev Pavel**  
Training getJavaJob   
http://www.getjavajob.com
